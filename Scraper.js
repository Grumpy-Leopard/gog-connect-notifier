'use strict';
const url = require('url');
const AWS = require('aws-sdk');
const SNS = require('./PushSNS');
const wr = require('./WebRequests');

const docClient = new AWS.DynamoDB.DocumentClient();
const snsarn = process.env.PUBLISH_DISCORD_SNSARN;
const dbTable = process.env.DDB_MAIN_DATA;

module.exports.handler = (event, context) => {
	wr.GET({hostname: 'www.gog.com', path: '/connect'}, (htmlreply) => {
		//console.log(htmlreply);
		let regex = RegExp('<span[^\>]*product-title__text[^\>]*>([^\<]*)<\/span><\/div>','g');
		let names = [];
		let match;
		let i = 0;
		while ((match = regex.exec(htmlreply)) !== null) {
			console.log("Matched: "+match)
			if (match[1] !== "") {
				names[i++] = match[1];
				console.log("Added : "+match[1]+" (Count: "+names.length+")")
			}
		}

		let params = {
			TableName: dbTable,
			Key: {
				'Key' : 'LastMatchedGames'
			}
		};
		console.log(params);
		
        docClient.get(params, (err, data) => {
			if (err) console.error('Unable to retrieve LastMatchedGames record ('+err+')')
			else {
				console.log(data);
				if (JSON.stringify(names) == data.Item.Value) console.log('No updates to game list.')
				else {
					console.log('Games have changed! '+names);

					const ddbClient = new AWS.DynamoDB();
					let putparams = {
						TableName: dbTable,
						Item: {
							Key: { S: 'LastMatchedGames' },
							Value: { S: JSON.stringify(names) },
						},
					};
					console.log(putparams);
					ddbClient.putItem(putparams, (err, resp) => {
						if (err) console.error(err, err.stack)
						else console.log(resp);
					});
					
					if (names.length > 0) {
						let content = {
							names: names,
						}

						params.Key = {'Key' : 'DiscordWebhooks'}
						console.log(params);
				
						docClient.get(params, (err, data) => {
							if (err) console.error('Unable to retrieve DiscordWebhooks record ('+err+')')
							else {
								console.log(data);
								let webhooks = JSON.parse(data.Item.Value);
								for (let i = 0, len = webhooks.length; i < len; i++) {
									checkDiscordWebhook(url.parse(webhooks[i]), (response) => {
										console.log(response);
										if (!response) {
										console.log('Webhook broken: '+webhooks[i])
										} else {
											content.webhook = webhooks[i];
											SNS.pushMessage(content, snsarn)
										}
									})
								}
							}
						})
					}

				}
			}
		})




	})
};

function checkDiscordWebhook(options, callback) {
	console.log(options);
	wr.GET(options, (response) => {
		console.log(response);
		let verification = JSON.parse(response);
		if ((verification.name) && (response.code == undefined)) {
			callback(true);
		} else {
			callback(false);
		}
	})
}

