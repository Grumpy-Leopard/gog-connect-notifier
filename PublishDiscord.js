'use strict';
const url = require('url');
const wr = require('./WebRequests');

module.exports.handler = (event, context) => {
    console.log(JSON.stringify(event));

    let alertContent = event.Records[0].Sns;
    let message = JSON.parse(alertContent.Message);
    console.log(message);

    if (message.names.length > 0 && message.webhook !== "") {
        let webhook = url.parse(message.webhook);
        webhook.headers = {
            'Content-Type': 'application/json',
        }

        let discordMessage = {
            username: 'GOG Connect Notifier',
            avatar_url: 'https://i.imgur.com/MEEd91T.png', // Uploaded specifically for this
            content: 'There are new games on GOG Connect!!',
            embeds: [{
                title: 'New Games on GOG Connect!',
                url: 'https://www.gog.com/connect',
                description: message.names.sort().join('\n'),
                color: 3186477,

                footer: {
                    text: 'Alerts by Grumpy Leopard',
                    icon_url: 'https://i.imgur.com/DIuP4RV.jpg',
                },
                timestamp: (new Date()).toISOString(),
            }],
        }

        wr.POST(webhook, JSON.stringify(discordMessage), (response) => {
        console.log(message);
        console.log(response);
        })
    }
}
